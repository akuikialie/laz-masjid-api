<?php

namespace App\Events;

use App\Models\User;

class UserForgotPassword extends Event
{
    public $user;
    /**
     * Create a new event instance.
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
}
