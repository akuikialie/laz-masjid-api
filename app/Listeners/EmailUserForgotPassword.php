<?php

namespace App\Listeners;

use App\Events\UserForgotPassword;
use App\Models\Mailer;

class EmailUserForgotPassword
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param UserForgotPassword $event
     */
    public function handle(UserForgotPassword $event)
    {
        $paramsEmail = [
            'mailer_module' => 'user:forgot-password',
            'mailer_from' => env('APP_NO_REPLY_EMAIL'),
            'mailer_to' => $event->user->user_email,
            'mailer_subject' => 'Forgot Password',
            'mailer_message' => 'pesan email',
        ];

        $mailer = new Mailer();
        $mailer->insertNewEmail($paramsEmail);
    }
}
