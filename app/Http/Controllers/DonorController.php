<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Donor;
use App\Http\Helper\Logger;

class DonorController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        config(['app.timezone' => 'Asia/Jakarta']);
    }

    public function add(Request $request)
    {
        Logger::debug($request, 'donor/add', 'request');

        $this->validate($request, [
            'donor_name' => 'required',
            'donor_phone' => 'required',
            'donor_address' => 'required',
            'donor_province' => 'required',
            'donor_city' => 'required',
            'organization_name' => 'required',
        ]);

        $donor = new Donor();
        $data = $donor->addDonor($request);

        Logger::debug(
            $request,
            'donor/add',
            'response',
            json_decode($data->getContent(), true)
        );

        return $data;
    }

    public function getAllDonors(Request $request)
    {
        Logger::debug($request, 'donor/all', 'request');
        $userKey = $request->header('key');

        $donor = new Donor();
        $data = $donor->getAllDonors($userKey);

        Logger::debug(
            $request,
            'donor/all',
            'response'
            //json_decode($data->getContent(), true)
        );

        return $data;
    }

    public function view(Request $request, $idUser)
    {
        Logger::debug($request, 'donor/view', 'request');

        $userKey = $request->header('key');

        $donor = new Donor();
        $data = $donor->getDonor($idUser);

        Logger::debug(
            $request,
            'donor/view',
            'response',
            json_decode($data->getContent(), true)
        );

        return $data;
    }

    public function update(Request $request, $donorId)
    {
        Logger::debug($request, 'donor/update', 'request');

        $donor = new Donor();
        $data = $donor->updateDonor($request, $donorId);

        Logger::debug(
            $request,
            'donor/update',
            'response',
            json_decode($data->getContent(), true)
        );

        return $data;
    }

    public function getSingleDonor(Request $request, $donaturId)
    {
    }
}
