<?php

namespace app\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\Validator;
use Illuminate\Http\Request;
use App\Http\Helper\Logger;

class Controller extends BaseController
{
    protected function formatValidationErrors(Validator $validator)
    {
        $errors = [
            'code' => 0,
            'message' => 'Invalid Request Parameter',
            'data' => $validator->errors()->getMessages(),
        ];

        return $errors;
    }

    protected function buildFailedValidationResponse(Request $request, array $errors)
    {
        Logger::debug($request, $this->parseUrl($request->url()), 'validate', $errors);

        if (isset(static::$responseBuilder)) {
            return call_user_func(static::$responseBuilder, $request, $errors);
        }

        return new JsonResponse($errors, 400);
    }

    private function parseUrl($url)
    {
        $parsing = explode('/', $url);

        return $parsing[3].'/'.$parsing[4];
    }
}
