<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Donation;
use App\Http\Helper\Logger;
use App\Http\Helper\DonationPaymentMethod;
use App\Http\Helper\DonationType;
use App\Http\Helper\MyResponse;

class DonationController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        config(['app.timezone' => 'Asia/Jakarta']);
    }

    public function getAllDonations(Request $request)
    {
        Logger::debug($request, 'donation/all', 'request');
        $userKey = $request->header('key');

        $donation = new Donation();
        $data = $donation->getAllDonations($userKey);

        Logger::debug(
            $request,
            'donation/all',
            'response'
            //json_decode($data->getContent(), true)
        );

        return $data;
    }

    public function getAllType(Request $request)
    {
        Logger::debug($request, 'donation/type', 'request');
        $userKey = $request->header('key');

        $donationType = DonationType::getAllDonationTypeLabel();

        $respondDonationType = null;
        foreach ($donationType as $k => $v) {
            $respondDonationType[] = [
                'id' => $k,
                'label' => $v,
            ];
        }
        Logger::debug(
            $request,
            'donation/type',
            'response'
            //json_decode($data->getContent(), true)
        );

        return (new MyResponse())->json(
            'Berhasil Mengambil Semua Tipe Donasi', 200, $respondDonationType, 1
        );
    }

    public function getAllPaymentMethod(Request $request)
    {
        Logger::debug($request, 'donation/payment', 'request');
        $userKey = $request->header('key');

        $donationPayment = DonationPaymentMethod::getAllPaymentMethodLabel();

        $respondDonationPayment = null;
        foreach ($donationPayment as $k => $v) {
            $respondDonationPayment[] = [
                'id_payment' => $k,
                'payment_name' => $v,
            ];
        }
        Logger::debug(
            $request,
            'donation/payment',
            'response'
            //json_decode($data->getContent(), true)
        );

        return (new MyResponse())->json(
            'Berhasil Mengambil Semua Jenis Pembayaran Donasi', 200, $respondDonationPayment, 1
        );
    }

    public function add(Request $request)
    {
        Logger::debug($request, 'donation/add', 'request');

        $this->validate($request, [
            'donation_type' => 'required',
            'donation_value' => 'required',
            'donation_payment' => 'required',
            'donation_donor_id' => 'required',
            'donation_user_id' => 'required',
            'donation_payment_status' => 'required',
            'donation_routine' => 'required',
        ]);

        $user = new Donation();
        $data = $user->addDonation($request);

        Logger::debug(
            $request,
            'donation/add',
            'response',
            json_decode($data->getContent(), true)
        );

        return $data;
    }
}
