<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Helper\Logger;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
    }

    public function profile(Request $request)
    {
        Logger::debug($request, 'user/profile', 'request');

        $userKey = $request->header('key');

        $user = new User();
        $data = $user->getProfile($userKey);

        Logger::debug(
            $request,
            'user/profile',
            'response',
            json_decode($data->getContent(), true)
        );

        return $data;
    }

    public function update(Request $request)
    {
        Logger::debug($request, 'user/update', 'request');

        $userKey = $request->header('key');

        $user = new User();
        $data = $user->updateUser($request, $userKey);

        Logger::debug(
            $request,
            'user/update',
            'response',
            json_decode($data->getContent(), true)
        );

        return $data;
    }

    public function getAllUser(Request $request)
    {
        Logger::debug($request, 'user/all', 'request');

        $userKey = $request->header('key');

        $user = new User();
        $data = $user->getAllUser($userKey);

        Logger::debug(
            $request,
            'user/all',
            'response',
            json_decode($data->getContent(), true)
        );

        return $data;
    }

    public function view(Request $request, $idUser)
    {
        Logger::debug($request, 'user/view', 'request');

        $userKey = $request->header('key');

        $user = new User();
        $data = $user->getUser($idUser);

        Logger::debug(
            $request,
            'user/view',
            'response',
            json_decode($data->getContent(), true)
        );

        return $data;
    }

    public function forgotPassword(Request $request)
    {
        Logger::debug($request, 'user/forgot', 'request');

        $user = new User();
        $data = $user->forgotPassword($request->email);

        Logger::debug(
            $request,
            'user/forgot',
            'response',
            json_decode($data->getContent(), true)
        );

        return $data;
    }
}
