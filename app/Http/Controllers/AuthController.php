<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Helper\Logger;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
    }

    public function register(Request $request)
    {
        Logger::debug($request, 'auth/register', 'request');

        $this->validate($request, [
            'organization_name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'password_confirm' => 'required|same:password',

        ]);

        $user = new User();
        $data = $user->register($request);

        Logger::debug(
            $request,
            'auth/register',
            'response',
            json_decode($data->getContent(), true)
        );

        return $data;
    }

    public function login(Request $request)
    {
        Logger::debug($request, 'auth/login', 'request');

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $user = new User();
        $data = $user->login($request);

        Logger::debug(
            $request,
            'auth/login',
            'response',
            json_decode($data->getContent(), true)
        );

        return $data;
    }

    public function logout(Request $request)
    {
        Logger::debug($request, 'auth/logout', 'request');

        $user = new User();
        $data = $user->logout($request);

        Logger::debug(
            $request,
            'auth/logout',
            'response',
            json_decode($data->getContent(), true)
        );

        return $data;
    }
}
