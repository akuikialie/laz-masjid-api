<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return response()->json([
        'code' => 1,
        'message' => 'Berhasil Membuka Project Sistem LAZ Masjid',
        'data' => [
            'app_version' => env('APP_VERSION'),
            'app_environment' => env('APP_ENVIRONMENT'),
            'lumen_version' => $app->version(),
        ],
    ]);
});
$app->group(['middleware' => [
    'AuthenticationValidation',
    'ContentValidation',
]], function () use ($app) {
    /* Auth */
    $app->post('auth/register', [
        'uses' => 'App\Http\Controllers\AuthController@register',
    ]);
    $app->post('auth/login', [
        'uses' => 'App\Http\Controllers\AuthController@login',
    ]);

    /* Users */
    $app->post('user/forgotPassword', [
        'uses' => 'App\Http\Controllers\UserController@forgotPassword',
    ]);
});

$app->group(['middleware' => [
    'AuthenticationUserValidation',
    'ContentValidation',
]], function () use ($app) {
    /* Donors */
    $app->post('donor/add', [
        'uses' => 'App\Http\Controllers\DonorController@add',
    ]);
    $app->post('donor/update/{donor_id}', [
        'uses' => 'App\Http\Controllers\DonorController@update',
    ]);

    /* Users */
    $app->post('user/update', [
        'uses' => 'App\Http\Controllers\UserController@update',
    ]);

    /* Donations */
    $app->post('donation/add', [
        'uses' => 'App\Http\Controllers\DonationController@add',
    ]);
});

$app->group(['middleware' => [
    'AuthenticationUserValidation',
]], function () use ($app) {
    /* Auth */
    $app->get('auth/logout', [
        'uses' => 'App\Http\Controllers\AuthController@logout',
    ]);

    /* Users */
    $app->get('user/profile', [
        'uses' => 'App\Http\Controllers\UserController@profile',
    ]);
    $app->get('user/all', [
        'uses' => 'App\Http\Controllers\UserController@getAllUser',
    ]);
    $app->get('user/{user_id}', [
        'uses' => 'App\Http\Controllers\UserController@view',
    ]);

    /* Donors */
    $app->get('donor/all', [
        'uses' => 'App\Http\Controllers\DonorController@getAllDonors',
    ]);
    $app->get('donor/{donor_id}', [
        'uses' => 'App\Http\Controllers\DonorController@view',
    ]);

    /* Donations */
    $app->get('donation/all', [
        'uses' => 'App\Http\Controllers\DonationController@getAllDonations',
    ]);
    $app->get('donation/type', [
        'uses' => 'App\Http\Controllers\DonationController@getAllType',
    ]);
    $app->get('donation/payment', [
        'uses' => 'App\Http\Controllers\DonationController@getAllPaymentMethod',
    ]);
});
