<?php

namespace app\Http\Middleware;

use Closure;
use App\Http\Helper\MyResponse;
use App\Models\User;

class AuthenticationUserValidation
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->header('key') == null) {
            return (new MyResponse())->json(
                'Invalid Key',
                401
            );
        }

        $user = new User();
        $getUser = $user->where('user_key', $request->header('key'))->first();

        if ($request->header('key') === null) {
            return (new MyResponse())->json(
                'Invalid Key Value',
                401
            );
        }

        return $next($request);
    }
}
