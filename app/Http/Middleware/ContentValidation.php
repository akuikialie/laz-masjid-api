<?php

namespace app\Http\Middleware;

use Closure;
use App\Http\Helper\MyResponse;

class ContentValidation
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->header('content-type') !== 'application/json') {
            return (new MyResponse())->json(
                'Invalid Request Content-Type',
                406
            );
        }

        if (count($request->all()) === 0) {
            return (new MyResponse())->json(
                'Invalid Format Content',
                406
            );
        }

        return $next($request);
    }
}
