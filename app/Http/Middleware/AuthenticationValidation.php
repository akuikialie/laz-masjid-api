<?php

namespace app\Http\Middleware;

use Closure;
use App\Http\Helper\MyResponse;

class AuthenticationValidation
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->header('x-api-key') == null) {
            return (new MyResponse())->json(
                'Invalid Global Key',
                401
            );
        }

        if ($request->header('x-api-key') !== env('APP_GLOBAL_KEY')) {
            return (new MyResponse())->json(
                'Invalid Global Key Value',
                401
            );
        }

        return $next($request);
    }
}
