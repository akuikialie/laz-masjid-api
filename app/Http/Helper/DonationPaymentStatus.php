<?php

namespace app\Http\Helper;

class DonationPaymentStatus
{
    public static $paymentStatusArray = [
        1 => 'paid',
        2 => 'waiting',
        3 => 'expired',
    ];

    public static function getPaymentStatusLabel($paymentId)
    {
        if (array_key_exists($paymentId, self::$paymentStatusArray)) {
            return self::$paymentStatusArray[$paymentId];
        }

        return;
    }

    public static function getAllPaymentStatusLabel()
    {
        return self::$paymentStatusArray;
    }
}
