<?php

namespace app\Http\Helper;

use Illuminate\Http\Request;
use Log;

class Logger
{
    public static function debug(
        Request $request,
        $moduleName = 'Global',
        $moduleMessage = 'Request',
        $data = null
    ) {
        $xApiKey = $request->header('x-api-key');
        $key = $request->header('key');
        $contentType = $request->header('content-type');
        $userAgent = $request->header('user-agent');

        $logDebug = [];
        $logDebug['header'] = [
            'content-type' => $contentType,
            'user-agent' => $userAgent,
            'key' => $key,
            'x-api-key' => $xApiKey,
        ];
        if ($data == null) {
            $logDebug['body'] = $request->all();
        } else {
            $logDebug['body'] = $data;
        }
        Log::debug($moduleName.' > '.$moduleMessage, $logDebug);
    }
}
