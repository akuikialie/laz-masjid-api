<?php
/**
 * Short description for MyResponse.php.
 *
 * @author Akku Iki Alie <achmadali1992@yahoo.com>
 *
 * @version 0.1
 *
 * @copyright (C) 2016 Akku Iki Alie <achmadali1992@yahoo.com>
 * @license Shoop E-Commerce License
 */

namespace App\Http\Helper;

class MyResponse
{
    public function json(
        $message = 'Default Response',
        $httpCode = 400,
        $data = [],
        $code = 0
    ) {
        return response()->json([
            'code' => $code,
            'message' => $message,
            'data' => $data,
        ], $httpCode);
    }
}
