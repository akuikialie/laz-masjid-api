<?php

namespace app\Http\Helper;

class DonationType
{
    public static $donationTypeArray = [
        1 => 'Zakat',
        2 => 'Infaq',
        3 => 'Shodaqoh',
        4 => 'Wakaf',
    ];

    public static function getDonationTypeLabel($typeId)
    {
        if (array_key_exists($typeId, self::$donationTypeArray)) {
            return self::$donationTypeArray[$typeId];
        }

        return;
    }

    public static function getAllDonationTypeLabel()
    {
        return self::$donationTypeArray;
    }
}
