<?php

namespace app\Http\Helper;

class DonationPaymentMethod
{
    public static $paymentMethodArray = [
        1 => 'COD',
        2 => 'Bank Transfer Mandiri Syariah',
        3 => 'Bank Transfer BNI Syariah',
        4 => 'Bank Transfer BRI Syariah',
    ];

    public static function getPaymentMethodLabel($paymentId)
    {
        if (array_key_exists($paymentId, self::$paymentMethodArray)) {
            return self::$paymentMethodArray[$paymentId];
        }

        return;
    }

    public static function getAllPaymentMethodLabel()
    {
        return self::$paymentMethodArray;
    }
}
