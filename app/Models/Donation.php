<?php

namespace app\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use App\Http\Helper\MyResponse;
use App\Http\Helper\DonationPaymentMethod;
use Illuminate\Http\Request;

class Donation extends Model implements
    AuthenticatableContract,
    AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $table = 'donations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'donation_type',
        'donation_value',
        'donation_payment',
        'donation_donor_id',
        'donation_user_id',
        'donation_organization_id',
        'donation_payment_status',
        'donation_routine',
    ];

    public function getAllDonations($userKey)
    {
        $donation = $this->join(
                                'organizations',
                                'organization_id',
                                '=',
                                'donations.donation_organization_id'
                            )
                         ->join(
                                'users',
                                'user_id',
                                '=',
                                'donations.donation_user_id'
                            )
                         ->join(
                                'donors',
                                'donors_id',
                                '=',
                                'donations.donation_donor_id'
                            )
                         ->orderBy('donation_id', 'desc')
                         ->get();

        if (!$donation) {
            return (new MyResponse())->json(
              'Data Donation tidak tersedia',
              400
          );
        }

        $donationData = null;
        foreach ($donation as $k => $v) {
            $paymentStr = DonationPaymentMethod::getPaymentMethodLabel($v->donation_payment);
            $routineStr = $v->donation_routine == 1 ? 'rutin' : 'insidental';

            $donationData[$k]['donation_id'] = $v->donation_id;
            $donationData[$k]['user_name'] = $v->user_name;
            $donationData[$k]['donor_name'] = $v->donor_name;
            $donationData[$k]['organization_name'] = $v->organization_name;
            $donationData[$k]['donation_value'] = $v->donation_value;
            $donationData[$k]['donation_payment'] = $v->donation_payment;
            $donationData[$k]['donation_payment_label'] = $paymentStr;
            $donationData[$k]['donation_payment_status'] = $v->donation_payment_status;
            $donationData[$k]['donation_type'] = $v->donation_type;
            $donationData[$k]['donation_routine'] = $v->donation_routine;
            $donationData[$k]['donation_routine_label'] = $routineStr;
        }

        return (new MyResponse())->json(
          'Berhasil Mengambil Semua Donation',
          200,
          $donationData,
          1
      );
    }

    public function addDonation(Request $request)
    {
        $user = new User();
        $userData = $user->where(
            'user_key',
            strtoupper($request->header('key'))
        )->first();

        $paramsInsert = [
            'donation_type' => $request->get('donation_type'),
            'donation_value' => $request->get('donation_value'),
            'donation_payment' => $request->get('donation_payment'),
            'donation_payment_status' => $request->get('donation_payment_status'),
            'donation_donor_id' => $request->get('donation_donor_id'),
            'donation_user_id' => $userData->user_id,
            'donation_organization_id' => $userData->user_organization_id,
            'donation_routine' => $request->get('donation_routine'),
        ];
        $insert = $this->create($paramsInsert);

        $data = $this->where('donation_id', $insert->id)->first();

        return (new MyResponse())->json(
            'Berhasil Tambah Donasi',
            200,
            $data,
            1
        );
    }
}
