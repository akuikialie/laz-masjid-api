<?php

namespace app\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Http\Request;
use DateTime;
use App\Http\Helper\MyResponse;
use App\Models\Organization;
use App\Events\UserForgotPassword;

class User extends Model implements
    AuthenticatableContract,
    AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_email',
        'user_password',
        'user_key',
        'user_level',
        'user_organization_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'user_password',
    ];

    public function register(Request $request)
    {
        $organization = new Organization();
        $organizationData = $organization->where(
            'organization_name',
            strtoupper($request->get('organization_name'))
        )->first();

        if ($organizationData == null) {
            $createOrganization = $organization->addOrganization($request);

            $organizationId = $createOrganization;
        } else {
            $organizationId = $organizationData->organization_id;
        }

        $data = $this->where('user_email', $request->get('email'))->first();

        if ($data !== null) {
            return (new MyResponse())->json(
                'Email sudah terdaftar',
                400
            );
        }

        $apiKey = md5((new DateTime())
            ->format('Y-m-d H:i:s').$request->get('email'));

        $paramsInsert = [
            'user_email' => $request->get('email'),
            'user_password' => md5($request->get('password')),
            'user_key' => $apiKey,
            'user_level' => 3,
            'user_organization_id' => $organizationId,
        ];
        $insert = $this->create($paramsInsert);

        $data = $this->where('user_id', $insert->id)->first();

        return (new MyResponse())->json(
            'Berhasil Register',
            200,
            $data,
            1
        );
    }

    public function login(Request $request)
    {
        $email = $request->get('email');
        $password = $request->get('password');

        $user = $this->where('user_email', $email)
                     ->join('organizations', 'organization_id', '=', 'users.user_organization_id')
                     ->join('user_levels', 'user_level_id', '=', 'users.user_level')
                     ->first();

        if ($user->user_key === null) {
            $apiKey = md5((new DateTime())
                ->format('Y-m-d H:i:s').$request->get('email'));

            $params['updated_at'] = (new DateTime())->format('Y-m-d H:i:s');
            $params['user_key'] = $apiKey;

            $this->where('user_email', $email)
                ->update($params);

            $user = $this->where('user_email', $email)
                 ->join('organizations', 'organization_id', '=', 'users.user_organization_id')
                 ->join('user_levels', 'user_level_id', '=', 'users.user_level')
                 ->first();
        }

        if (!$user) {
            return (new MyResponse())->json(
                'Email tidak tersedia',
                400
            );
        }

        $dateUpdate = new Datetime($user->updated_at);
        $dateNow = new Datetime();
        $intervalHours = $dateUpdate->diff($dateNow);

        $hours = $intervalHours->h;
        $hours = $hours + ($intervalHours->days * 24);

        if ($user->user_is_login === 1 && $hours > 4) {
            /* Force update user_is_login : 0 */
            $params['user_is_login'] = 0;
            $params['updated_at'] = (new DateTime())->format('Y-m-d H:i:s');

            $this->where('user_email', $email)
                 ->update($params);
        }

        if ($user->user_is_login === 1) {
            return (new MyResponse())->json(
                'Akun sedang login',
                400
            );
        }

        if (md5($password) === $user->user_password) {
            $params['user_is_login'] = 1;

            $this->where('user_email', $email)
                 ->update($params);

            $user = $this->where('user_email', $email)
                 ->join('organizations', 'organization_id', '=', 'users.user_organization_id')
                 ->join('user_levels', 'user_level_id', '=', 'users.user_level')
                 ->first();

            return (new MyResponse())->json(
                'Berhasil Login',
                200,
                $user,
                1
            );
        }

        return (new MyResponse())->json(
            'Email tidak sesuai',
            400
        );
    }

    public function logout(Request $request)
    {
        $userKey = $request->header('key');

        $user = $this->where('user_key', $userKey)
                     ->first();

        if ($user->user_is_login === 1) {
            $params['user_is_login'] = 0;

            $this->where('user_key', $userKey)
                 ->update($params);

            $user = $this->where('user_key', $userKey)
                     ->first();

            return (new MyResponse())->json(
                    'Berhasil Logout',
                    200,
                    $user,
                    1
                );
        }

        return (new MyResponse())->json(
            'Akun tidak sedang login',
            200
        );
    }

    public function getProfile($userKey)
    {
        $user = $this->where('user_key', $userKey)
                     ->join('organizations', 'organization_id', '=', 'users.user_organization_id')
                     ->join('user_levels', 'user_level_id', '=', 'users.user_level')
                     ->first();

        if (!$user) {
            return (new MyResponse())->json(
                'User tidak tersedia',
                400
            );
        }

        return (new MyResponse())->json(
            'Berhasil Mengambil Profile',
            200,
            $user,
            1
        );
    }

    public function updateUser(Request $request, $userKey)
    {
        $params = $request->all();
        $params['updated_at'] = (new DateTime())->format('Y-m-d H:i:s');

        $this->where('user_key', $userKey)
             ->update($params);

        $user = $this->where('user_key', $userKey)
                     ->join('organizations', 'organization_id', '=', 'users.user_organization_id')
                     ->first();

        if ($user === false) {
            return (new MyResponse())->json(
                'User tidak tersedia',
                400
            );
        }

        return (new MyResponse())->json(
            'Berhasil Memperbaharui User',
            200,
            $user,
            1
        );
    }

    public function getAllUser($userKey)
    {
        $user = $this->where('user_level', '!=', 1)
                     ->orWhereNull('user_level')
                     ->join('organizations', 'organization_id', '=', 'users.user_organization_id')
                     ->join('user_levels', 'user_level_id', '=', 'users.user_level')
                     ->orderBy('user_id', 'desc')
                     ->get();

        if (!$user) {
            return (new MyResponse())->json(
                'User Administrator tidak tersedia',
                400
            );
        }

        return (new MyResponse())->json(
            'Berhasil Mengambil Semua User',
            200,
            $user,
            1
        );
    }

    public function getUser($userId)
    {
        $user = $this->where('user_id', $userId)
                     ->join('organizations', 'organization_id', '=', 'users.user_organization_id')
                     ->join('user_levels', 'user_level_id', '=', 'users.user_level')
                     ->first();

        if (!$user) {
            return (new MyResponse())->json(
                'User tidak tersedia',
                400
            );
        }

        return (new MyResponse())->json(
            'Berhasil Mengambil Profile',
            200,
            $user,
            1
        );
    }

    public function forgotPassword($userEmail)
    {
        $user = $this->where('user_email', $userEmail)
                     ->join('user_levels', 'user_level_id', '=', 'users.user_level')
                     ->first();

        if (!$user) {
            return (new MyResponse())->json(
                'User tidak tersedia',
                400
            );
        }

        event(new UserForgotPassword($user));

        $params['user_is_login'] = 0;
        $params['updated_at'] = (new DateTime())->format('Y-m-d H:i:s');

        $this->where('user_email', $userEmail)
             ->update($params);

        return (new MyResponse())->json(
            'Berhasil Mengirim Email Forgot Password',
            200,
            $user,
            1
        );
    }
}
