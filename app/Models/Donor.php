<?php

namespace app\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Http\Request;
use App\Http\Helper\MyResponse;
use Datetime;

class Donor extends Model implements
    AuthenticatableContract,
    AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $table = 'donors';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'donor_name',
        'donor_email',
        'donor_phone',
        'donor_address',
        'donor_province',
        'donor_city',
        'donor_organization_id',
    ];

    public function addDonor(Request $request)
    {
        $organization = new Organization();
        $organizationData = $organization->where(
            'organization_name',
            strtoupper($request->get('organization_name'))
        )->first();

        if ($organizationData == null) {
            $createOrganization = $organization->addOrganization($request);

            $organizationId = $createOrganization;
        } else {
            $organizationId = $organizationData->organization_id;
        }

        $newDonor = $this->create([
            'donor_name' => $request->get('donor_name'),
            'donor_email' => $request->get('donor_email'),
            'donor_organization_id' => $organizationId,
            'donor_phone' => $request->get('donor_phone'),
            'donor_address' => $request->get('donor_address'),
            'donor_province' => strtoupper($request->get('donor_province')),
            'donor_city' => strtoupper($request->get('donor_city')),
        ]);

        $data = $this->where('donors_id', $newDonor->id)->first();

        return (new MyResponse())->json(
            'Berhasil Tambah Donatur',
            200,
            $data,
            1
        );
    }

    public function getAllDonors($userKey)
    {
        $donor = $this->join('organizations', 'organization_id', '=', 'donors.donor_organization_id')
                   ->orderBy('donors_id', 'desc')
                   ->get();

        if (!$donor) {
            return (new MyResponse())->json(
              'User Administrator tidak tersedia',
              400
          );
        }

        return (new MyResponse())->json(
          'Berhasil Mengambil Semua Donatur',
          200,
          $donor,
          1
      );
    }

    public function getDonor($donorId)
    {
        $donor = $this->where('donors_id', $donorId)
                      ->join('organizations', 'organization_id', '=', 'donors.donor_organization_id')
                      ->first();

        if (!$donor) {
            return (new MyResponse())->json(
              'Donatur tidak tersedia',
              400
          );
        }

        return (new MyResponse())->json(
          'Berhasil Mengambil Donatur',
          200,
          $donor,
          1
        );
    }

    public function updateDonor(Request $request, $donorId)
    {
        $params = $request->all();
        $donor = $this->where('donors_id', $donorId)
                      ->first();

        if ($donor === null) {
            return (new MyResponse())->json(
                'Donatur tidak tersedia',
                400
            );
        }

        $params['updated_at'] = (new DateTime())->format('Y-m-d H:i:s');
        $params['donor_city'] = strtoupper($request->donor_city);
        $params['donor_province'] = strtoupper($request->donor_province);

        $this->where('donors_id', $donorId)
             ->update($params);

        $donor = $this->where('donors_id', $donorId)
                      ->join('organizations', 'organization_id', '=', 'donors.donor_organization_id')
                      ->first();

        if ($donor === null) {
            return (new MyResponse())->json(
              'Donatur tidak tersedia',
              400
            );
        }

        return (new MyResponse())->json(
            'Berhasil Memperbaharui Donatur',
            200,
            $donor,
            1
        );
    }
}
