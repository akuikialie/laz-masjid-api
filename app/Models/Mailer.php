<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class Mailer extends Model
{
    protected $table = 'mailers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'mailer_module',
        'mailer_from',
        'mailer_to',
        'mailer_subject',
        'mailer_message',
    ];

    public function insertNewEmail($params)
    {
        $params = [
            'mailer_module' => strtoupper($params['mailer_module']),
            'mailer_from' => $params['mailer_from'],
            'mailer_to' => $params['mailer_to'],
            'mailer_subject' => $params['mailer_subject'],
            'mailer_message' => $params['mailer_message'],
        ];

        $insert = $this->create($params);

        return $insert->id;
    }
}
