<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class CreateTableMailers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.  *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
   		$table = $this->table('mailers', ['id' => 'mailer_id']);
        $table->addColumn('mailer_module', 'string', ['limit' => 255])
              ->addColumn('mailer_from', 'string', ['limit' => 255])
              ->addColumn('mailer_to', 'string', ['limit' => 255])
              ->addColumn('mailer_bcc', 'string', ['limit' => 255, 'null' => true])
              ->addColumn('mailer_cc', 'string', ['limit' => 255, 'null' => true])
              ->addColumn('mailer_subject', 'string', ['limit' => 255])
              ->addColumn('mailer_message', 'text', ['limit' => MysqlAdapter::TEXT_LONG])
              ->addColumn('mailer_delivered_status', 'integer', ['limit' => 1, 'default' => 0])
              ->addColumn('mailer_delivered', 'datetime', ['null' => true])
              ->addColumn('mailer_try_send', 'integer', ['limit' => 1, 'default' => 0])
              ->addColumn('created_at', 'datetime')
              ->addColumn('updated_at', 'datetime', ['null' => true])
              ->addIndex(['mailer_from'])
              ->addIndex(['mailer_to'])
              ->addIndex(['mailer_module'])
              ->addIndex(['mailer_subject'])
              ->addIndex(['mailer_delivered_status'])
              ->addIndex(['created_at'])
              ->create();
    }
}
