<?php

use Phinx\Migration\AbstractMigration;

class CreateUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('users', ['id' => 'user_id']);
        $table->addColumn('user_name', 'string', ['limit' => 255])
              ->addColumn('user_password', 'string', ['limit' => 255])
              ->addColumn('user_phone', 'string', ['limit' => 15, 'null' => true])
              ->addColumn('user_email', 'string', ['limit' => 255])
              ->addColumn('user_key', 'string', ['limit' => 255, 'null' => true])
              ->addColumn('user_organization_id', 'integer', ['limit' => 10, 'null' => true])
              ->addColumn('user_is_login', 'integer', ['limit' => 1, 'default' => 0])
              ->addColumn('user_level', 'string', ['limit' => 25])
              ->addColumn('created', 'datetime')
              ->addColumn('modified', 'datetime', ['null' => true])
              ->addIndex(['user_email'])
              ->addIndex(['user_key'])
              ->addIndex(['user_organization_id'])
              ->addIndex(['user_level'])
              ->create();
    }
}
