<?php

use Phinx\Migration\AbstractMigration;

class InsertUserLevels extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->execute("INSERT INTO user_levels (user_level_id, level_name, level_description) VALUES ('1', 'SUPERADMIN', NULL);");
        $this->execute("INSERT INTO user_levels (user_level_id, level_name, level_description) VALUES ('2', 'ADMINISTRATOR', NULL);");
        $this->execute("INSERT INTO user_levels (user_level_id, level_name, level_description) VALUES ('3', 'PENGELOLA', NULL);");
        $this->execute("INSERT INTO user_levels (user_level_id, level_name, level_description) VALUES ('4', 'TAMIR', NULL);");
    }
}
