<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class CreateOrganizations extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('organizations', ['id' => 'organization_id']);
        $table->addColumn('organization_name', 'string', ['limit' => 255])
              ->addColumn('organization_address', 'text', ['limit' => MysqlAdapter::TEXT_REGULAR, 'null' => true])
              ->addColumn('organization_phone', 'string', ['limit' => 15, 'null' => true])
              ->addColumn('organization_email', 'string', ['limit' => 255, 'null' => true])
              ->addColumn('organization_province', 'string', ['limit' => 255, 'null' => true])
              ->addColumn('organization_city', 'string', ['limit' => 255, 'null' => true])
              ->addColumn('created', 'datetime')
              ->addColumn('modified', 'datetime', ['null' => true])
              ->addIndex('organization_email', ['type' => 'fulltext'])
              ->addIndex('organization_name', ['type' => 'fulltext'])
              ->create();
    }
}
