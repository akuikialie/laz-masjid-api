<?php

use Phinx\Migration\AbstractMigration;

class CreateDonations extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
	    $table = $this->table('donations', ['id' => 'donation_id']);
        $table->addColumn('donation_type', 'string', ['limit' => 255])
              ->addColumn('donation_value', 'integer', ['limit' => 200, 'null' => true])
              ->addColumn('donation_payment', 'integer', ['limit' => 10])
              ->addColumn('donation_donor_id', 'integer', ['limit' => 10])
              ->addColumn('donation_user_id', 'integer', ['limit' => 10])
              ->addColumn('donation_organization_id', 'integer', ['limit' => 10])
              ->addColumn('donation_payment_status', 'string', ['limit' => 100])
              ->addColumn('donation_routine', 'integer', ['limit' => 1, 'default' => 0])
              ->addColumn('created_at', 'datetime')
              ->addColumn('updated_at', 'datetime', ['null' => true])
              ->addIndex(['donation_type'])
              ->addIndex(['donation_payment'])
              ->addIndex(['donation_payment_status'])
              ->addIndex(['donation_organization_id'])
              ->addIndex(['donation_user_id'])
              ->addIndex(['donation_donor_id'])
              ->addIndex(['donation_routine'])
              ->create();
    }
}
