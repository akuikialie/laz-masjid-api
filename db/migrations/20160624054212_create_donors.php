<?php

use Phinx\Migration\AbstractMigration;

class CreateDonors extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
		$table = $this->table('donors', ['id' => 'donors_id']);
        $table->addColumn('donor_name', 'string', ['limit' => 255])
              ->addColumn('donor_phone', 'string', ['limit' => 15, 'null' => true])
              ->addColumn('donor_email', 'string', ['limit' => 255])
              ->addColumn('donor_address', 'string', ['limit' => 255])
              ->addColumn('donor_province', 'string', ['limit' => 255])
              ->addColumn('donor_city', 'string', ['limit' => 255])
              ->addColumn('donor_organization_id', 'integer', ['limit' => 10, 'null' => true])
              ->addColumn('created_at', 'datetime')
              ->addColumn('updated_at', 'datetime', ['null' => true])
              ->addIndex(['donor_email'])
              ->addIndex(['donor_name'])
              ->addIndex(['donor_phone'])
              ->addIndex(['donor_organization_id'])
              ->create();
    }
}
