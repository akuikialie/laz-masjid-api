<?php


class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     */
    public function testExample()
    {
        $this->get('/');

        $this->seeJsonEquals([
            'code' => 1,
            'message' => 'Berhasil Membuka Project Sistem LAZ Masjid',
            'data' => [
                'app_version' => 'beta1.0',
                'app_environment' => 'development',
                'lumen_version' => 'Lumen (5.2.7) (Laravel Components 5.2.*)',
            ],
        ]);
    }
}
